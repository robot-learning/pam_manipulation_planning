# pam_manipulation_planning

This is a package for motion and manipulation planning for a mobile robot to deliver legged objects.

Author: roya.sabbaghnovin@utah.edu

### How to set up? ###

- Set a simulation example in "Examples_sim.py" or choose one from the available ones. (Running real experiments requiers the lab setup as described in the paper)
 
- Assign the example number in "main_simulation.py" file as chosen.

- Run "launch_simulation.launch".


